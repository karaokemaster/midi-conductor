dotnet publish -f netcoreapp3.1 -c Release -r osx-x64

export PATH=~/.local/bin:$PATH
export OUTPUT_DIR=`cd ./bin/Release/netcoreapp3.1/osx-64/publish 2> /dev/null && pwd -P`
gtk-mac-bundler ./MacBundle/KaraokeMaster.MidiConductor.bundle
