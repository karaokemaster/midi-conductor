using System;
using System.Threading;
using System.Threading.Tasks;

namespace KaraokeMaster.MidiConductor
{
    public class BeatScheduler
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly TimeSpan _oneMinute = TimeSpan.FromMinutes(1);

        private readonly Timer _beatTimer;
        // TODO: Add configuration options
        private readonly TimeSpan _baseLatency = TimeSpan.FromMilliseconds(10);
        private readonly TimeSpan _offsetThreshold = TimeSpan.FromMilliseconds(5);
        
        private DateTime _nextBeat;
        private double _tempo;

        public BeatScheduler()
        {
            _beatTimer = new Timer(async obj =>
            {
                _nextBeat += BeatInterval;
                if (BeatHandler != null)
                    await BeatHandler();
            }, null, TimeSpan.FromMilliseconds(-1), TimeSpan.Zero);
        }
        
        public TimeSpan Latency { get; set; }
        
        public Func<Task> BeatHandler { get; set; }

        private double Tempo
        {
            get => _tempo;
            set
            {
                if (_tempo == value) return;

                _tempo = value;
                while (_tempo > 170) _tempo /= 2;
                while (_tempo < 70) _tempo *= 2;
                
                BeatInterval = _oneMinute / _tempo;
                Log.Debug($"Tempo changed to {_tempo} (adjusted from {value})");
            }
        }

        private TimeSpan BeatInterval { get; set; }
        
        public async Task Beat(double tempo)
        {
            Log.Debug("Beat received");
            if (Latency == TimeSpan.Zero && BeatHandler != null)
                await BeatHandler();
            
            // Calculate "actual" beat time, and set the tempo to get the interval between beats
            // TODO: Temper sudden BPM changes/out-of-range values
            Tempo = tempo;
            var totalLatency = Latency - _baseLatency;
            var beatTime = DateTime.Now - totalLatency;

            // Calculate when the next beat "should" be due
            var nextBeat = beatTime + BeatInterval;
            while (nextBeat < DateTime.Now) nextBeat += BeatInterval;
            
            // Compare to next beat due
            var offset = (nextBeat - _nextBeat).Duration();
            Log.Debug($"Offset from current schedule {offset:c}");
            if (offset < _offsetThreshold)
            {
                // There's not enough difference between beats yet, so don't do anything
                // Should probably at least reschedule or somehow compensate the next time...
                return;
            }

            // Reschedule timer
            _nextBeat = nextBeat;
            var dueIn = beatTime - DateTime.Now;
            Log.Debug($"Rescheduling; next beat due in {dueIn:c}");
            _beatTimer.Change(dueIn, BeatInterval);
            
            // If we're REALLY far off, we should take action
            if (offset > BeatInterval / 2)
            {
                // Action... send beat now???
            }
        }
    }
}