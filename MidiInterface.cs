/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Commons.Music.Midi;

namespace KaraokeMaster.MidiConductor
{
    public class MidiInterface
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private IMidiOutput _output;
        private Timer _timer;

        public MidiInterface(string name)
        {
            Setup(name);
        }

        ~MidiInterface()
        {
            _output.Send(new[] { MidiEvent.MidiStop }, 0, 1, 0);
            
            _output.CloseAsync();
        }
        
        public static Dictionary<string, string> Devices => MidiAccessManager.Default.Outputs.ToDictionary(o => o.Name, o => o.Id);

        private async void Setup(string name)
        {
            var access = MidiAccessManager.Default;
            var outputDetails = access.Outputs.First(o => o.Name == name);
            _output = await access.OpenOutputAsync(outputDetails.Id);
            
            _output.Send(new[] { MidiEvent.MidiStart }, 0, 1, 0);
            
        }
        
        public async Task SendBeat(double tempo)
        {
            for (int i = 0; i < 24; i++)
            {
                _output.Send(new[] {MidiEvent.MidiClock}, 0, 1, 0);
            }

            return;
            
            if (_timer == null)
            {
                _timer = new Timer(o => { _output.Send(new[] {MidiEvent.MidiClock}, 0, 1, 0); }, null, TimeSpan.Zero, TimeSpan.Zero);
            }

            if (tempo < 1)
            {
                _timer?.Dispose();
                _timer = null;
                return;
            }

            _timer.Change(TimeSpan.Zero, TimeSpan.FromTicks((long)(TimeSpan.TicksPerMinute / 24.0 / tempo)));
        }

        public async Task SendCue(int index)
        {
            if (index > 127 || index < 0) 
                throw new ArgumentOutOfRangeException(nameof(index), index, $"{nameof(index)} must be between 0 and 127 (value provided {index})");

            byte note = (byte)(index % 128);

            _output.Send(new byte [] {MidiEvent.NoteOn, note, 0x70}, 0, 3, 0); // There are constant fields for each MIDI event
            await Task.Delay(TimeSpan.FromMilliseconds(20));
            _output.Send(new byte [] {MidiEvent.NoteOff, note, 0x70}, 0, 3, 0);
        }

        public async Task SendValue(int channel, byte value)
        {
            if (channel > 119 || channel < 0) 
                throw new ArgumentOutOfRangeException(nameof(channel), channel, $"{nameof(channel)} must be between 0 and 119 (value provided {channel})");
            if (value > 127)
                throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(value)} must be between 0 and 127 (value provided {value})");

            byte note = (byte)(channel % 128);
            value = (byte)(value % 128);
            
            _output.Send(new byte [] {MidiEvent.CC, note, value}, 0, 3, 0); // There are constant fields for each MIDI event
        }
    }
}