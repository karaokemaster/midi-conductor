/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;

namespace KaraokeMaster.MidiConductor
{
    /// <summary>
    /// Calculates exponential moving average for a value, including variance measures
    /// (Adapted from https://nestedsoftware.com/2018/04/04/exponential-moving-average-on-streaming-data-4hhl.24876.html)
    /// </summary>
    public class ExponentialMovingAverage
    {
        private readonly double _alpha;
        private double _mean;
        private double _variance;
        
        public ExponentialMovingAverage(double alpha, double mean)
        {
            _alpha = alpha;
            _mean = mean;
            _variance = 0;
        }

        public double Mean => _mean;
        
        public double Beta => 1 - _alpha;

        public double StdDev => Math.Sqrt(_variance);
        
        public double Update(double newValue)
        {
            var redistributedMean = Beta * _mean;
            var meanIncrement = _alpha * newValue;
            var newMean = redistributedMean + meanIncrement;
            // JS: const varianceIncrement = this.alpha * (newValue - this.mean)**2
            var varianceIncrement = _alpha * Math.Pow(newValue - _mean, 2);
            var newVariance = Beta * (_variance + varianceIncrement);
            
            _mean = newMean;
            _variance = newVariance;

            return newMean;
        }
    }
}