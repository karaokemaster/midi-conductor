/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using GLib;
using Application = Gtk.Application;
using FileInfo = GLib.FileInfo;

[assembly: log4net.Config.XmlConfigurator(Watch = true, ConfigFile = "log4net.config")]
namespace KaraokeMaster.MidiConductor
{
    class Program
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            var builder = new Autofac.ContainerBuilder();
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            var container = builder.Build();
            var scope = container.BeginLifetimeScope();
            
            var app = new Application("org.KaraokeMaster.MidiConductor",
                GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            var cueDialog = scope.Resolve<CueDialog>();
            var logDialog = scope.Resolve<LogDialog>();
            var win = scope.Resolve<MainWindow>();
            app.AddWindow(logDialog);
            app.AddWindow(cueDialog);
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}