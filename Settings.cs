/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Claunia.PropertyList;
using KaraokeMaster.Domain.Models;

namespace KaraokeMaster.MidiConductor
{
    public class Settings
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private const string FILE_NAME = "Settings.plist";
        
        public string MidiDevice { get; set; }
        public string BaseUrl { get; set; }
        public List<Cue> Cues { get; set; }

        public Action OnUpdate { get; set; }
        
        public void Load()
        {
            var file = new FileInfo(FILE_NAME);
            if (!file.Exists) return;

            var rootDict = (NSDictionary) PropertyListParser.Parse(file);

            var midiDevice = rootDict.ObjectForKey("MidiDevice") as NSString;
            var baseUrl = rootDict.ObjectForKey("BaseUrl") as NSString;
            var cues = rootDict.ObjectForKey("Cues") as NSArray;

            if (!string.IsNullOrEmpty(midiDevice?.Content))
                MidiDevice = midiDevice.Content;
            
            if (!string.IsNullOrEmpty(baseUrl?.Content))
                BaseUrl = baseUrl.Content;

            Cues = new List<Cue>();
                
            if (cues != null)
            {
                foreach (var cue in cues)
                {
                    var cueDict = (NSDictionary)cue;
                    var q = new Cue
                    {
                        Index = (byte) (cueDict.ObjectForKey("Index") as NSNumber).ToInt(),
                        Name = (cueDict.ObjectForKey("Name") as NSString)?.Content,
                        Theme = (cueDict.ObjectForKey("Theme") as NSNumber).ToBool(),
                        Color = (cueDict.ObjectForKey("Color") as NSString)?.Content
                    };

                    Cues.Add(q);
                }
            }
        }

        public void Save()
        {
            var file = new FileInfo(FILE_NAME);
            
            var root = new NSDictionary();
            
            if (!string.IsNullOrEmpty(MidiDevice))
                root.Add("MidiDevice", MidiDevice);
            if (!string.IsNullOrEmpty(BaseUrl))
                root.Add("BaseUrl", BaseUrl);

            var cues = new NSArray(Cues.Select(cue =>
            {
                var q = new NSDictionary
                {
                    {"Index", cue.Index}, 
                    {"Name", cue.Name}, 
                    {"Theme", cue.Theme}, 
                    {"Color", cue.Color}
                };
                return (NSObject)q;
            }).ToArray());
            root.Add("Cues", cues);

            PropertyListParser.SaveAsXml(root, file);

            OnUpdate?.Invoke();
        }
    }
}