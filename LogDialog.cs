/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Globalization;
using System.Linq;
using System.Net;
using Gdk;
using Gtk;
using log4net.Core;
using log4net.Util;
using UI = Gtk.Builder.ObjectAttribute;

namespace KaraokeMaster.MidiConductor
{
    public class LogDialog : Dialog
    {
        [UI] private Button _done = null;
        [UI] private TreeView _logEvents = null;
        [UI] private ComboBox _logLevel = null;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private static readonly IconTheme theme = new IconTheme();

        private static readonly Pixbuf DebugIcon   = theme.LoadIcon("dialog-question",    16, IconLookupFlags.UseBuiltin);
        private static readonly Pixbuf ErrorIcon   = theme.LoadIcon("dialog-error",       16, IconLookupFlags.UseBuiltin);
        private static readonly Pixbuf FatalIcon   = theme.LoadIcon("process-stop",       16, IconLookupFlags.UseBuiltin);
        private static readonly Pixbuf InfoIcon    = theme.LoadIcon("dialog-information", 16, IconLookupFlags.UseBuiltin);
        private static readonly Pixbuf WarningIcon = theme.LoadIcon("dialog-warning",     16, IconLookupFlags.UseBuiltin);

        private ListStore _eventList;
        private TreeModelFilter _eventListFilter;
        
        public LogDialog() : this(new Builder("MainWindow.glade"))
        {
            
        }

        private LogDialog(Builder builder) : base(builder.GetObject("LogDialog").Handle)
        {
            builder.Autoconnect(this);

            Title = "Log";
            
            ((log4net.Repository.Hierarchy.Hierarchy)Log.Logger.Repository).Root.AddAppender(new LogAppender(this));

            ConfigureList();
            LoadLevels();
            
            _logLevel.Changed += LogLevelOnChanged;
            DeleteEvent += OnDeleteEvent;    
            _done.Clicked += DoneOnClicked;
        }

        public void AddEntry(LoggingEvent logEvent)
        {
            _eventList.AppendValues(logEvent);

            TreeIter iter;
            var model = _logEvents.Model;
            var count = model.IterNChildren();
            if (count < 1) return;
            
            model.IterNthChild(out iter, count - 1);
            var path = _logEvents.Model.GetPath(iter);
            
            _logEvents.ScrollToCell(path, _logEvents.Columns[0], false, 0, 0);
        }
        
        private void OnDeleteEvent(object o, DeleteEventArgs args)
        {
            Hide();
        }

        private void DoneOnClicked(object? sender, EventArgs e)
        {
            Hide();
        }

        private void LogLevelOnChanged(object? sender, EventArgs e)
        {
            _eventListFilter.Refilter();
        }

        private void ConfigureList()
        {
            _logEvents.Selection.Mode = SelectionMode.None;

            var eventTypeColumn = new TreeViewColumn {Title = ""};
            var eventTypeCell = new CellRendererPixbuf();
            eventTypeColumn.PackStart (eventTypeCell, true);
            eventTypeColumn.SetCellDataFunc (eventTypeCell, RenderEventType);
            _logEvents.AppendColumn (eventTypeColumn);
            
            var timestampColumn = new TreeViewColumn {Title = "Timestamp"};
            var timestampCell = new CellRendererText ();
            timestampColumn.PackStart (timestampCell, true);
            timestampColumn.SetCellDataFunc (timestampCell, RenderTimestamp);
            _logEvents.AppendColumn (timestampColumn);
            
            var messageColumn = new TreeViewColumn {Title = "Message"};
            var messageCell = new CellRendererText ();
            messageColumn.PackStart (messageCell, true);
            messageColumn.SetCellDataFunc (messageCell, RenderMessage);
            _logEvents.AppendColumn (messageColumn);
            
            var threadColumn = new TreeViewColumn {Title = "Thread"};
            var threadCell = new CellRendererText ();
            threadColumn.PackStart (threadCell, true);
            threadColumn.SetCellDataFunc (threadCell, RenderThread);
            _logEvents.AppendColumn (threadColumn);
            
            var loggerColumn = new TreeViewColumn {Title = "Logger"};
            var loggerCell = new CellRendererText ();
            loggerColumn.PackStart (loggerCell, true);
            loggerColumn.SetCellDataFunc (loggerCell, RenderLogger);
            _logEvents.AppendColumn (loggerColumn);

            // Create a model that will hold two strings
            _eventList = new ListStore (typeof (LoggingEvent));

            // Create the filter for our treeview
            _eventListFilter = new TreeModelFilter(_eventList, null) {VisibleFunc = IsEntryVisible};

            // Assign the model to the TreeView
            _logEvents.Model = _eventListFilter;
        }

        private bool IsEntryVisible(ITreeModel model, TreeIter iter)
        {
            try
            {
                TreeIter tree;
                _logLevel.GetActiveIter(out tree);
                var level = (Level) _logLevel.Model.GetValue (tree, 0);

                var entry = (LoggingEvent)model.GetValue(iter, 0);

                if (level == null || entry == null)
                    return true;
                
                if (entry.Level >= level)
                    return true;
            }
            catch (Exception e)
            {
                Log.Error("Error filtering log display", e);
            }

            return false;
        }

        private void LoadLevels()
        {
            _logLevel.Clear();
            
            var cell = new CellRendererText ();
            _logLevel.SetCellDataFunc(cell, RenderLevel);
            _logLevel.PackStart (cell, true);
            // _logLevel.SetAttributes (cell, "text", 0);
            
            var store = new ListStore(typeof (Level));
            _logLevel.Model = store;

            store.AppendValues(Level.Debug);
            store.AppendValues(Level.Info);
            store.AppendValues(Level.Warn);
            store.AppendValues(Level.Error);
            store.AppendValues(Level.Fatal);

            _logLevel.Active = 1;
        }

        private void RenderLevel (ICellLayout cell_layout, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var level = (Level) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = level.Name;
        }
        
        private void RenderEventType (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var logEvent = (LoggingEvent) model.GetValue (iter, 0);
            Pixbuf pixbuf;

            if (logEvent == null)
                return;
            
            if (logEvent.Level == Level.Info)
                pixbuf = InfoIcon;
            else if (logEvent.Level == Level.Warn)
                pixbuf = WarningIcon;
            else if (logEvent.Level == Level.Debug)
                    pixbuf = DebugIcon;
            else if (logEvent.Level == Level.Error)
                    pixbuf = ErrorIcon;
            else if (logEvent.Level == Level.Fatal)
                    pixbuf = FatalIcon;
            else
                    return;

            (cell as CellRendererPixbuf).Pixbuf = pixbuf;
        }

        private void RenderTimestamp (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var logEvent = (LoggingEvent) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = logEvent?.TimeStamp.ToString("hh:mm:ss.fff", CultureInfo.InvariantCulture);
        }

        private void RenderLogger (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var logEvent = (LoggingEvent) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = logEvent?.LoggerName;
        }

        private void RenderThread (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var logEvent = (LoggingEvent) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = logEvent?.ThreadName;
        }

        private void RenderMessage (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var logEvent = (LoggingEvent) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = logEvent?.RenderedMessage;
        }
    }
}