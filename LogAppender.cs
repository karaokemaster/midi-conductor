/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using log4net.Appender;
using log4net.Core;

namespace KaraokeMaster.MidiConductor
{
	/// <summary>
	/// Displays messages in log list
	/// </summary>
	/// <remarks>
	/// Displays each LoggingEvent as an entry in the log list
	/// </remarks>
	public class LogAppender : AppenderSkeleton
	{
		private readonly LogDialog _dialog;
		
		public LogAppender(LogDialog dialog)
		{
			_dialog = dialog;
		}

		protected override void Append(LoggingEvent loggingEvent)
		{

			_dialog.AddEntry(loggingEvent);
		} 
	}
}