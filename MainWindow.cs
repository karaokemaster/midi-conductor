/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Gdk;
using Gtk;
using KaraokeMaster.Domain.Models;
using UI = Gtk.Builder.ObjectAttribute;
using Microsoft.AspNetCore.SignalR.Client;
using Window = Gtk.Window;

namespace KaraokeMaster.MidiConductor
{
    class MainWindow : Window
    {
        [UI] private ComboBox _midiDevice = null;
        [UI] private Entry _url = null;
        [UI] private Button _urlSubmit = null;
        [UI] private Label _connectionStatusLabel = null;
        [UI] private LevelBar _connectionStatus = null;
        [UI] private Label _beatStatusLabel = null;
        [UI] private LevelBar _beatStatus = null;
        [UI] private Label _bpm = null;
        [UI] private Button _editCues = null;
        [UI] private Button _showLog = null;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly Settings _settings;
        private CueDialog _cueDialog;
        private LogDialog _logDialog;
        private HubConnection _connection;
        private MidiInterface _midiInterface;
        private readonly BeatScheduler _beatScheduler = new BeatScheduler();
        private bool _playing;
        private Timer _idleTimer;
        private Stopwatch _latencyStopwatch;
        private Timer _latencyTimer;
        private ExponentialMovingAverage _latencyAverage;

        public MainWindow(Settings settings, CueDialog cueDialog, LogDialog logDialog) : this(new Builder("MainWindow.glade"))
        {
            TypeHint = WindowTypeHint.Utility;
            Resizable = false;
            Modal = true;

            _settings = settings;
            _cueDialog = cueDialog;
            _logDialog = logDialog;
            
            _cueDialog.SendCue = SendCue;
        }

        private MainWindow(Builder builder) : base(builder.GetObject("MainWindow").Handle)
        {
            builder.Autoconnect(this);

            TypeHint = WindowTypeHint.Utility;
            Resizable = false;
            Modal = true;

            DeleteEvent += Window_DeleteEvent;
            Shown += OnShown;

            _midiDevice.Changed += MidiDeviceOnChanged;
            _urlSubmit.Clicked += UrlSubmitOnClicked;
            
            _editCues.Clicked += EditCuesOnActivated;
            _showLog.Clicked += ShowLogOnClicked;

            _beatScheduler.BeatHandler += ScheduledBeatHandler;
        }

        private void OnShown(object? sender, EventArgs e)
        {
            _url.Text = _settings.BaseUrl ?? string.Empty;
            
            LoadInterfaces();
            StartConnection();
        }

        private async void UrlSubmitOnClicked(object? sender, EventArgs e)
        {
            var url = _url.Text;
            
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                // BAD url; should probably tell someone.
                return;
            }

            Log.Info($"KaraokeMaster base url updated to {url}");
            _settings.BaseUrl = url;
            _settings.Save();

            await Disconnect();
            StartConnection();
        }

        private void ShowLogOnClicked(object? sender, EventArgs e)
        {
            _logDialog.ShowNow();
        }

        private void EditCuesOnActivated(object? sender, EventArgs e)
        {
            _cueDialog.ShowNow();
        }

        private void MidiDeviceOnChanged(object? sender, EventArgs e)
        {
            TreeIter tree;
            _midiDevice.GetActiveIter(out tree);
            var selectedDevice = (String) _midiDevice.Model.GetValue (tree, 0);
            Log.Info($"MIDI device selection changed to {selectedDevice}");
            _settings.MidiDevice = selectedDevice;
            _settings.Save();
            
            _midiInterface = new MidiInterface(selectedDevice);
        }

        private async void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            await Disconnect();
            
            Application.Quit();
        }

        private void LoadInterfaces()
        {
            _midiDevice.Clear();
            
            var cell = new CellRendererText ();
            _midiDevice.PackStart (cell, true);
            _midiDevice.SetAttributes (cell, "text", 0);
            
            var store = new ListStore(typeof (string));
            _midiDevice.Model = store;
            var devices = MidiInterface.Devices.Select(d => d.Key).ToArray();
            devices.ToList().ForEach(d => store.AppendValues(d));

            if (!string.IsNullOrEmpty(_settings.MidiDevice))
            {
                var index = Array.IndexOf(devices, _settings.MidiDevice);
                if (index >= 0)
                    _midiDevice.Active = index;

                Log.Info($"MIDI device in settings {_settings.MidiDevice}; selection {(index >= 0 ? "succeeded" : "failed")}");
            }
        }

        private async void StartConnection()
        {
            _connectionStatus.Value = 0;
            _beatStatus.Value = 0;

            if (!Uri.IsWellFormedUriString(_settings.BaseUrl, UriKind.Absolute))
            {
                // TODO: Tell the user?
                Log.Fatal($"Invalid KaraokeMaster base URL specified ({_settings.BaseUrl}); aborting.");
                return;
            }

            var url = new Uri(new Uri(_settings.BaseUrl), "hub");

            Log.Info($"Connecting to KaraokeMaster API {url}");

            _connection = new HubConnectionBuilder()
                .WithUrl(url)
                // TODO: Implement IRetryPolicy to be more smarter
                .WithAutomaticReconnect()
                .Build();

            try
            {
                await Task.Delay(2000);
                await _connection.StartAsync();
                _connectionStatus.Value = 1;
                Log.Info("Connected!");
            }
            catch (Exception ex)
            {
                // TODO: Tell the user?
                Log.Error($"Failed to connect to {url}", ex);
                return;
            }

            _connection.On<double>("Beat", BeatReceived);

            _connection.On<bool>("PlayerState", PlayerStateReceived);

            _connection.On<long, bool>("BeatEcho", EchoReceived);

            // TODO: We should make sure we're filtering out "special" entries (e.g. 0 and 1)
            _connection.On<int>("SendCue", SendCue);

            _connection.On("RequestCues", RequestCuesReceived);

            _settings.OnUpdate = async () => { await RequestCuesReceived(); };
            
            await CheckLatency();
        }

        private async Task EchoReceived(long thenTimestamp, bool alive)
        {
            _latencyStopwatch?.Stop();
            
            _latencyTimer?.Dispose();
            _latencyTimer = null;
            
            if (alive)
            {
                // We should only ever be sending echoes with alive == true...
                Log.Warn($"WHY TF ARE WE GETTING ANOTHER BEAT ECHO?! {thenTimestamp}");
                return;
            }

            var nowTimestamp = Stopwatch.GetTimestamp();
            var roundTripTicks = (_latencyStopwatch?.ElapsedTicks ?? long.MaxValue) * 0.01 / TimeSpan.TicksPerMillisecond;
            var roundTripTime = _latencyStopwatch?.ElapsedMilliseconds ?? long.MaxValue;
            
            if (_latencyAverage == null)
                _latencyAverage = new ExponentialMovingAverage(0.3, roundTripTicks);
            else
                _latencyAverage.Update(roundTripTicks);

            Log.Debug($"Received BeatEcho response with timestamp {thenTimestamp} (elapsed round trip time {roundTripTime} ms; now timestamp {nowTimestamp})");
            Log.Info($"BeatEcho RTT {roundTripTicks:0.00} ms; average {_latencyAverage.Mean:0.00}; std.dev. {_latencyAverage.StdDev:0.00}");
            
            if (_latencyTimer != null)
            {
                // The timer should get destroyed before the echo returns, so this should never be true...
                Log.Warn($"Latency timer still active, check all the things.");
                return;
            }

            var recheckIn = TimeSpan.FromSeconds(15); // TimeSpan.FromMinutes(1);
            // If this latency >> avg, check sooner?
            // If variance/stddev is high then check sooner?
            // Maybe by default check less often, though what's the harm?
            _latencyTimer = new Timer(async state =>
            {
                await CheckLatency();
                _latencyTimer?.DisposeAsync();
                _latencyTimer = null;
            }, null, recheckIn, TimeSpan.FromMilliseconds(-1));
        }
        
        private async Task PlayerStateReceived(bool playing)
        {
            if (_playing == playing) return;

            _playing = playing;

            Log.Info($"Player state changed: {playing}");

            if (_midiInterface == null) return;

            if (_playing)
            {
                Log.Info($"Lighting cue: Playing");
                await _midiInterface.SendCue(1);
                _idleTimer?.Dispose();
                _idleTimer = null;
            }
            else
            {
                _idleTimer = new Timer(async state =>
                {
                    Log.Info($"Lighting cue: Idle");
                    await _midiInterface.SendCue(0);
                    _idleTimer?.Dispose();
                    _idleTimer = null;
                }, null, TimeSpan.FromSeconds(2), TimeSpan.Zero);
            }
        }

        private async Task BeatReceived(double tempo)
        {
            Log.Debug($"BEAT {tempo:0}");

            _bpm.Text = tempo.ToString("0");
            Task.Run(async () =>
            {
                _beatStatus.Value = 1;
                await _beatScheduler.Beat(tempo);

                _midiInterface?.SendBeat(tempo);

                await Task.Delay(35);
                _beatStatus.Value = 0;
            });
        }

        private async Task RequestCuesReceived()
        {
            var cues = _settings.Cues.ToArray();

            await _connection.InvokeAsync<Cue[]>("UpdateCues", cues);
        }

        private async Task ScheduledBeatHandler()
        {
            // TODO: Remove need for passing tempo
            _midiInterface?.SendBeat(0);
        }

        private async Task SendCue(int index)
        {
            await _midiInterface.SendCue(index);
        }

        private async Task Disconnect()
        {
            if (_connection != null)
                await _connection.DisposeAsync();
            if (_latencyTimer != null)
                await _latencyTimer.DisposeAsync();
            if (_idleTimer != null)
                await _idleTimer.DisposeAsync();
            
            _latencyStopwatch?.Stop();
            
            _latencyStopwatch = null;
            _idleTimer = null;
            _latencyTimer = null;
            _latencyAverage = null;
        }
        
        private async Task CheckLatency()
        {
            Log.Info($"Initiating beat detection network latency check...");
            
            _latencyStopwatch = new Stopwatch();
            var timestamp = Stopwatch.GetTimestamp();

            _latencyStopwatch.Start();
            await _connection.InvokeAsync("BeatEcho", timestamp, true);

            Log.Debug($"Sent BeatEcho request with timestamp {timestamp} (elapsed call time {_latencyStopwatch.ElapsedMilliseconds} ms)");

            _latencyTimer = new Timer(async state =>
            {
                Log.Warn($"BeatEcho not received after 15 seconds!");
                await CheckLatency();
            }, null, TimeSpan.FromSeconds(15), TimeSpan.FromMilliseconds(-1));
        }
    }
}