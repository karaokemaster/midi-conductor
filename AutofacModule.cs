using Autofac;
using log4net;
using System.Reflection;
using Module = Autofac.Module;

namespace KaraokeMaster.MidiConductor
{
    public class AutofacModule : Module
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c =>
            {
                var s = new Settings();
                s.Load();
                return s;
            }).SingleInstance();

            builder.RegisterType<CueDialog>();
            builder.RegisterType<LogDialog>();
            builder.RegisterType<MainWindow>();

            builder.RegisterType<MidiInterface>();
            builder.RegisterType<BeatScheduler>();
        }
    }
}
