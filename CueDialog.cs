/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gtk;
using KaraokeMaster.Domain.Models;
using UI = Gtk.Builder.ObjectAttribute;

namespace KaraokeMaster.MidiConductor
{
    public class CueDialog : Dialog
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly Settings _settings;
        private readonly ListStore _cueList;
        
        [UI] private Button _ok = null;
        [UI] private Button _cancel = null;
        [UI] private TreeView _cueMappings = null;
        
        public Func<int, Task> SendCue { get; set; }
        
        public CueDialog(Settings settings) : this(new Builder("MainWindow.glade"))
        {
            _settings = settings;
        }

        private CueDialog(Builder builder) : base(builder.GetObject("CueDialog").Handle)
        {
            builder.Autoconnect(this);

            Title = "Edit Cues";

            _cueMappings.ActivateOnSingleClick = true;
            
            var noteIndexColumn = new TreeViewColumn {Title = "#"};
            var noteIndexCell = new CellRendererText ();
            noteIndexColumn.PackStart (noteIndexCell, true);
            noteIndexColumn.SetCellDataFunc (noteIndexCell, RenderIndex);
            _cueMappings.AppendColumn (noteIndexColumn);
            
            var sendColumn = new TreeViewColumn {Title = "Test"};
            var sendCell = new CellRendererPixbuf();
            sendCell.Pixbuf = IconTheme.Default.LoadIcon("audio-speakers", 10,
                IconLookupFlags.UseBuiltin | IconLookupFlags.GenericFallback);
            sendColumn.PackStart(sendCell, false);
            _cueMappings.AppendColumn(sendColumn);

            var cueNameColumn = new TreeViewColumn {Title = "Cue Name"};
            var cueNameCell = new CellRendererText {Editable = true};
            cueNameCell.Edited += CueNameCellOnEdited;
            cueNameColumn.PackStart (cueNameCell, true);
            cueNameColumn.SetCellDataFunc (cueNameCell, RenderCueName);
            _cueMappings.AppendColumn (cueNameColumn);
            
            var themeColumn = new TreeViewColumn {Title = "Theme"};
            var themeCell = new CellRendererToggle {Activatable = true, Radio = false};
            themeCell.Toggled += ThemeCellOnToggled;
            themeColumn.PackStart (themeCell, true);
            themeColumn.SetCellDataFunc (themeCell, RenderTheme);
            _cueMappings.AppendColumn (themeColumn);
            
            var colorColumn = new TreeViewColumn {Title = "Color"};
            var colorCell = new CellRendererText {Editable = true};
            colorCell.Edited += ColorCellOnEdited;
            colorColumn.PackStart (colorCell, true);
            colorColumn.SetCellDataFunc (colorCell, RenderColor);
            _cueMappings.AppendColumn (colorColumn);

            _cueMappings.RowActivated += CueMappingsOnRowActivated;
            // Create a model that will hold two strings
            _cueList = new ListStore (typeof (Cue));

            // Assign the model to the TreeView
            _cueMappings.Model = _cueList;
            
            DeleteEvent += OnDeleteEvent;
            Shown += OnShown;
            
            _ok.Clicked += OkOnClicked;
            _cancel.Clicked += CancelOnClicked;
        }

        private void CueMappingsOnRowActivated(object o, RowActivatedArgs args)
        {
            if (args.Column.Title != "Test") return;
            
            TreeIter iter;
            _cueList.GetIter (out iter, args.Path);

            var cue = (Cue) _cueList.GetValue(iter, 0);
            SendCue?.Invoke(cue.Index);
        }

        private void OnShown(object? sender, EventArgs e)
        {
            LoadCues();
        }

        private void OkOnClicked(object? sender, EventArgs e)
        {
            SaveCues();
            Hide();
        }

        private void CancelOnClicked(object? sender, EventArgs e)
        {
            LoadCues();
            Hide();
        }

        private void CueNameCellOnEdited(object o, EditedArgs args)
        {
            TreeIter iter;
            _cueList.GetIter (out iter, new Gtk.TreePath (args.Path));

            var cue = (Cue) _cueList.GetValue(iter, 0);
            cue.Name = args.NewText;
        }

        private void ThemeCellOnToggled(object o, ToggledArgs args)
        {
            TreeIter iter;
            _cueList.GetIter (out iter, new Gtk.TreePath (args.Path));

            var cue = (Cue) _cueList.GetValue(iter, 0);
            cue.Theme = !cue.Theme;
        }

        private void ColorCellOnEdited(object o, EditedArgs args)
        {
            TreeIter iter;
            _cueList.GetIter (out iter, new Gtk.TreePath (args.Path));

            var cue = (Cue) _cueList.GetValue(iter, 0);
            cue.Color = args.NewText;
        }

        private void OnDeleteEvent(object o, DeleteEventArgs args)
        {
            LoadCues();
            Hide();
        }

        private void RenderIndex (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            var cue = (Cue) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = cue.Index.ToString();
        }

        private void RenderCueName (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, ITreeModel model, Gtk.TreeIter iter)
        {
            var cue = (Cue) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererText).Text = cue.Name;
            (cell as Gtk.CellRendererText).Editable = (cue.Index >= 2);
        }

        private void RenderTheme (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, ITreeModel model, Gtk.TreeIter iter)
        {
            var cue = (Cue) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererToggle).Active = cue.Theme;
            (cell as Gtk.CellRendererToggle).Activatable = (cue.Index >= 2);
        }

        private void RenderColor (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, ITreeModel model, Gtk.TreeIter iter)
        {
            var cue = (Cue) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererText).Text = "";
            (cell as Gtk.CellRendererText).Editable = (cue.Index >= 2);
        }
        
        private void LoadCues()
        {
            _cueList.Clear();

            _cueList.AppendValues(new Cue { Index = 0, Name = "Idle" });
            _cueList.AppendValues(new Cue { Index = 1, Name = "Playing" });
            Enumerable
                .Range(2, 128)
                .Select(i => _settings.Cues.FirstOrDefault(q => q.Index == i) ?? new Cue {Index = (byte) i})
                .ToList()
                .ForEach(cue => _cueList.AppendValues(cue));
        }

        private void SaveCues()
        {
            var cues = new List<Cue>();

            _cueList.Foreach((model, path, iter) =>
            {
                var cue = (Cue) model.GetValue(iter, 0);
                cues.Add(cue);
                return false;
            });

            _settings.Cues = cues
                .OrderBy(c => c.Index)
                .Where(c => c.Index > 1 && !string.IsNullOrWhiteSpace(c.Name) && (c.Theme || !string.IsNullOrWhiteSpace(c.Color)))
                .ToList();
            _settings.Save();
        }
    }
}